export interface EmployeeDataInterface {
  id: number,
  firstName: string,
  lastName: string,
  email: string,
  dateOfBirth: string,
  education: object,
  company: string,
  experience: number,
  package: number
}