import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeeDataInterface } from '../interface/employee-data.interface';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  constructor(private http: HttpClient) {}

  addEmployee(data: EmployeeDataInterface): Observable<object> {
    return this.http.post('http://localhost:3000/employees', data)
  }

  getEmployeeList(): Observable<object[]> {
    return this.http.get<EmployeeDataInterface[]>('http://localhost:3000/employees')
  }

  updateEmployee(id: number, data: EmployeeDataInterface): Observable<object> {
    return this.http.put(`http://localhost:3000/employees/${id}`, data)
  }

  deleteEmployee(id: number) {
    return this.http.delete(`http://localhost:3000/employees/${id}`)
  }
}
