import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EducationInterface } from '../interface/education.interface';
import { EmployeeDataInterface } from '../interface/employee-data.interface';
import { EmployeeService } from '../service/employee.service';
import { SnackService } from '../service/snack.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  employeeForm: FormGroup

  constructor(private formBuilder: FormBuilder, private employeeService: EmployeeService, private dialogRef: MatDialogRef<EmployeeComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private notification: SnackService) {
    this.employeeForm = this.formBuilder.group({
      firstName: '',
      lastName: '',
      email: '',
      dateOfBirth: '',
      education: '',
      company: '',
      experience: '',
      package: ''
    })
  }
  ngOnInit(): void {
    this.employeeForm.patchValue(this.data)
  }
  education: EducationInterface[] = [
    { value: 'funtamental-0', viewValue: 'Ensino Fundamental' },
    { value: 'médio-1', viewValue: 'Ensino Médio' },
    { value: 'superior-2', viewValue: 'Ensino Superior' },
  ];

  employeeData: EmployeeDataInterface = {
    id: 0,
    firstName: '',
    lastName: '',
    email: '',
    dateOfBirth: '',
    education: this.education,
    company: '',
    experience: 0,
    package: 0
  }

  onFormSubmit() {
    if (this.employeeForm.valid) {
      if (this.data) {
        this.employeeService.updateEmployee(this.data.id, this.employeeForm.value).subscribe({
          next: () => {
            this.notification.openSnackBar('Funcionário atualizado com sucesso.')
            this.dialogRef.close(true)
          },
          error: (err) => console.error(err)
        })
      } else {
        this.employeeService.addEmployee(this.employeeForm.value).subscribe({
          next: () => {
            this.notification.openSnackBar('Funcionário salvo com sucesso.')
            this.dialogRef.close(true)
          },
          error: (err) => console.error(err)
        })
      }

    }
  }
}
