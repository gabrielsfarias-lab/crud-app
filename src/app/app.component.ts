import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeService } from './service/employee.service';
import { SnackService } from './service/snack.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'email', 'dateOfBirth', 'education', 'company', 'experience', 'package', 'action'];
  dataSource!: MatTableDataSource<object>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngOnInit(): void {
    this.getEmployeeList()
  }
  constructor(private dialog: MatDialog, private employeeService: EmployeeService, private notification: SnackService) {}

  openEmployeeForm() {
    const dialogRef = this.dialog.open(EmployeeComponent)
    dialogRef.afterClosed().subscribe({
      next: (response => {
        if (response) this.getEmployeeList()
      })
    })
  }

  getEmployeeList() {
    this.employeeService.getEmployeeList().subscribe({
      next: (response) => {
        this.dataSource = new MatTableDataSource(response);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator
      },
      error: console.error,
    })
  }

  deleteEmployee(id: number) {
    this.employeeService.deleteEmployee(id).subscribe({
      next: () => {
        this.notification.openSnackBar('Funcionário apagado com sucesso.')
        this.getEmployeeList()
      },
      error: console.error
    })
  }

  openEmployeeEditForm(data: unknown) {
    const dialogRef = this.dialog.open(EmployeeComponent, { data })
    dialogRef.afterClosed().subscribe({
      next: (response => {
        if (response) this.getEmployeeList()
      })
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
